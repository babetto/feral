close all
clear
clc

K = load('SlotMatrix.txt');

[theta_a] = calc_winding_axis(3, K(:,1))

theta_d = 30;

RotorAlignment = theta_a - theta_d