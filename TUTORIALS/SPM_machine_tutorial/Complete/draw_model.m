close all
clear 
clc

%% Open new FEMM instance
openfemm
newdocument(0);

%% Load model data
MODEL_DATA
MD.ModelName = 'SPM_motor_to_complete';

%% Draw stator
draw_stator(MD);

%% Import the rotor geometry
build_dxf2fem_model({'dxf/SPM_rotor'}, MD, 0, 0, 0, 1);

%% Add here other operations ...