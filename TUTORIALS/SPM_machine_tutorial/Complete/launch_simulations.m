close all
clear 
clc

%% Single no-load simulation 
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.SaveDensityPlots = 1
sim_var_rotor_position(MD, SD);

clear SD

%% Single no-load simulation (rotating parts)
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.MoveMotionGroups = 1; 
SD.SaveDensityPlots = 1;
sim_var_rotor_position(MD, SD);

clear SD

%% Single no-load simulation (rotating parts + figure improvement)
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.MoveMotionGroups = 1; 
SD.SaveDensityPlots = 1;
SD.DensityPlotGray = 0;
SD.DensityPlotLegend = 0;
SD.DensityPlotMax = 1.8;
SD.DensityPlotMin = 0;
% SD.HideContourPlot = 0;
SD.ImageCrop = 1;
% SD.ImageExtension = 'png';
SD.ImageTransparency = 1;

sim_var_rotor_position(MD, SD);

clear SD

%% No-Load simulation (0-60deg)
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'no_load';

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

clear SD

%% No-Load simulation (0-60deg) extended plots
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'no_load';
SD.PlotResults = 1; % enable results plotting
SD.SaveFigures = 1; % enable figures saving
SD.CompletePeriod = 1; % enable completion of the electric period

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

clear SD

%% No-Load cogging torque
MODEL_DATA

SD.CurrentAmplitude = 0;
SD.CurrentAngle = 0;
SD.RotorPositions = [0:0.25:10-0.25];
SD.FileResultsPrefix = 'cogging';
SD.PlotResults = 1;
SD.SaveFigures = 1;

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

clear SD

%% No-Load Model data iron losses
MODEL_DATA

SD.CurrentAmplitude = 0;
SD.CurrentAngle = 0;
SD.RotorPositions = [0:3:180-3]/MD.PolePairs;
SD.FileResultsPrefix = 'fft_ironlosses';
SD.IronLossesFFT = 1; % compute fft losses
SD.MirrorHalfPeriod = 1; % extend the electric period
SD.SaveMeshNodes = 1; % save the mesh nodes
SD.SaveMeshElementsValues = 1; % save the element properties
SD.TempID = 'FFT'; % rename the temporary folder name
SD.MechSpeedRPM = 3000; % [rpm] mechanical speed 

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

clear SD

%% Single on-load simulation (rotating parts + figure improvement + 2plots)
MODEL_DATA

SD.CurrentAmplitude = 20; % [A]
SD.CurrentAngle = 90; % [deg]
SD.MoveMotionGroups = 1; 
SD.SaveDensityPlots = 1; % enable plot density saving
SD.DensityPlotType = {'bmag','jmag'}; % plot both flux and current density plots
SD.DensityPlotGray = [0, 1]; % bmag is colored, jmag has gray scale
SD.DensityPlotLegend = [0, 0]; % no legend
SD.DensityPlotMax = [2, 3]; % maximum values for bmag and jmag
SD.DensityPlotMin = [0, 0]; % minimum values for bmag and jmag
SD.ImageCrop = 1;
SD.ImageExtension = 'png';
SD.ImageTransparency = 1;

sim_var_rotor_position(MD, SD);

clear SD

%% On-load simulation (0-60deg) find MTPA
clear SD, clc
MODEL_DATA

SD.CurrentAmplitude = 20; % [A]
SD.CurrentAngleRange = [90:100]; % [deg]

% find the MTPA current angle
mtpa = sim_find_mtpa(MD, SD);

SD.CurrentAngle = mtpa.CurrentAngle; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'on_load';
SD.PlotResults = 1;
SD.SaveFigures = 1;
SD.MechSpeedRPM = 2000; % [rpm]

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

clear SD

%% On-load simulation (0-60deg) extended
MODEL_DATA

SD.CurrentAmplitude = 20; % [A]
SD.CurrentAngleRange = [90:100]; % [deg] 

% find the MTPA current angle
mtpa = sim_find_mtpa(MD, SD);

SD.CurrentAngle = mtpa.CurrentAngle; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.FileResultsPrefix = 'on_load';
SD.PlotResults = 1;
SD.SaveFigures = 1;
SD.MechSpeedRPM = 2000; % [rpm]
SD.CompletePeriod = 1;

[AvgValues, VecValues] = sim_var_rotor_position(MD, SD);

clear SD

%% On-load simulation (0-60deg) with rotor skewing
MODEL_DATA

SD.CurrentAmplitude = 20; % [A]
SD.CurrentAngleRange = [90:100]; % [deg]
 
% find the MTPA current angle
mtpa = sim_find_mtpa(MD, SD);

SD.CurrentAngle = mtpa.CurrentAngle; % [deg]
SD.RotorPositions = [0:3:60-3]/MD.PolePairs; % [deg]
SD.Skew_vec = [-10/3 0 10/3]; % [deg]
SD.FileResultsPrefix = 'on_load';
SD.PlotResults = 1;
SD.SaveFigures = 1;
SD.MechSpeedRPM = 2000; % [rpm]
SD.CompletePeriod = 1;

[AvgValues, VecValues, SkewValues] = sim_var_rotor_position(MD, SD);

clear SD

%% Mapping
MODEL_DATA

SD.Id_vec = -28:2:0; % [A]
SD.Iq_vec = 0:2:28; % [A]
%SD.RotorPositions = [0:3:60]/MD.PolePairs;
%SD.Skew_vec = [0];
SD.TempID = 'map';
SD.ClearTempFolder = 1;
SD.PlotResults = 1;


MyFirstMap = sim_mapping(MD, SD);

clear SD