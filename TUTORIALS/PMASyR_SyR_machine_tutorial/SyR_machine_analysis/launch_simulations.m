close all
clear 
clc

%% Single no-load simulation 
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 0; % [A]
SD.CurrentAngle = 0; % [deg]
sim_var_rotor_position(MD, SD);

%% Single no-load simulation only Id
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 60; % [A]
SD.CurrentAngle = 90; % [deg]
SD.MoveMotionGroups = 1; % enable physical rotor rotation
SD.SaveDensityPlots = 1; % enable picture saving
SD.DensityPlotLegend = 0; % remove legend
SD.ImageCrop = 1; % enable image crop
SD.ImageTransparency = 1; % remove white background
sim_var_rotor_position(MD, SD);

%% Single no-load simulation only Iq
clc, clear SD
MODEL_DATA

SD.CurrentAmplitude = 60; % [A]
SD.CurrentAngle = 9; % [deg]
sim_var_rotor_position(MD, SD);

