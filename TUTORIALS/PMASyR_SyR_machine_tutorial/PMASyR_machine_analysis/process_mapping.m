close all; clear; clc;

load(['results\','map_Id_[0-10-80]_A_Iq_[0-10-80]_A_thme_0_deg_20190806T220102.mat'])

%% INPUT DATA
Map.VoltageLimit = 54; % rated voltage (V) peak
% Map.WindingResistance = 0.02; % winding resistance [ohm]
Map.EndWindingInductance = 0; % end-winding inductance [H]
Map.CurrentLimit = 80; % rated current (A) peak
Map.MaxMechSpeedRPM = 20e3; % maximum mechanical speed (rpm)
Map.InterpMethod = 'cubic'; % interpolation method
Map.CurrentAmplitudeRange = [10:10:80]; % current amplitudes array for the MTPA locus
Map.CurrentAngleRange = linspace(0,90,1e3); % current angle array to compute FW and MTPV
Map.DQReferenceFrame = 'SyR'; % reference frame of the simulation

%% Maximum torque per ampere (MTPA) locus
[mtpa, base] = calc_mtpa(Map, Map.CurrentAmplitudeRange, Map.CurrentAngleRange, Map.VoltageLimit);
                                
%% Flux-weakening (FW) and Maximum torque per volt (MTPV) locus 
[Map.MTPA, Map.FW, Map.MTPV] = calc_mtpa_fw_mtpv(Map, Map.CurrentAmplitudeRange);

%% FIGUREs
% Set the outputs to plot and to save
% Outputs on the y-axis
Y = { 'Torque',             'MechPower',    ...
      'CurrentAmplitude',   'InputPower',   ...
      'ReactPower',         'Losses',       ...
      'Phi', 'PowerFactor', 'Efficiency'};

% Outputs on the x-axis (always mechanical speed)
X = repmat({'MechSpeedRPM'}, 1, length(Y));

% Plot results
plot_sim_mapping_results(Map, X, Y);


