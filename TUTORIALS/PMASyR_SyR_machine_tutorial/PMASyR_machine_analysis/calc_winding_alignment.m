close all
clear
clc

% load the slot matrix 
K = load('..\SlotMatrix.txt');

% compute the angular positions of the a-axis (1st column)
[theta_a] = calc_winding_axis(2, K(:,1), 5) % [deg]

% d-axis angular position (SyR reference frame)
theta_d = 0; % [deg]

RotorAlignment = (theta_a - theta_d) 