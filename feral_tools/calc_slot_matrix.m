function SlotMatrix  = calc_slot_matrix(NumPhases, PolePairs, Slots, Chording, NumLayers, MatrixShift, PlotStarSlot)

if nargin < 7
  PlotStarSlot = 0;
end

% Define short variables
m = NumPhases;
pp = PolePairs;
Q = Slots; 

% Slot/pole/phase
q = Q/(2*pp)/m;

% Slot pitch mech. angle [deg] 
SlotAngle = 360/Q;

% Slot pitch el. angle [deg]
ElSlotAngle = pp*SlotAngle;

% Winding periodicity
t = gcd(Q, pp);

% Coil pitch
Yq = floor(Q/2/pp);
if Yq < 1
  Yq = 1;
end

% Chorded coil through
Yqc = Yq - Chording;

%% Check winding feasibility
if (Q/m/t ~= floor(Q/t/m)) && NumLayers == 1
  warning('Winding unfeasible!')
  SlotMatrix = NaN;
  return
end

% Compute phasor angles in the range [0 ... 360) el. deg
Phasors = exp(1i*(0:Q-1)*ElSlotAngle*pi/180);
PhasorAngles = atan2d(imag(Phasors), real(Phasors));
PhasorAngles = (round(PhasorAngles*10000)/10000);
idx_neg = find(PhasorAngles<0);
PhasorAngles(idx_neg) = PhasorAngles(idx_neg) + 360;

%% Compute one layer of the slot matrix
k1 = zeros(Q, m); % 1-st layer matrix
for idx_phase = 1:m
    
    for kk = 0:1
      ang_min = 360/m * (idx_phase-1) + kk*180;
      if ang_min >= 360
        ang_min = ang_min - 360;
      end
      ang_max = ang_min + 180/m;
      idx = find((PhasorAngles >= ang_min) & (PhasorAngles < ang_max));
      k1(idx, idx_phase) =  0.5 * (-1)^kk;
    end    
   
end

%% Compute the second layer matrix by shifting the first one
k2 = circshift(k1, Yqc);

%% Compute the complete slot matrix
K = k1 - k2;

%% Rotate slot matrix in order to have a determined alignment
SlotMatrix = circshift(K, MatrixShift)

if PlotStarSlot

  figure
  hold on

  for ph = 0:m-1 
    ang = ph*60 + [0, 180];
    plot(cosd(ang), sind(ang), '--', 'linewidth', 3)
  end

  for ang = PhasorAngles
    plot([0 cosd(ang)], [0 sind(ang)], 'o')
    text(cosd(ang), sind(ang),  num2str(ang), 'fontsize', 20)
  end

  axis equal

end
