function kw = calc_winding_factor(PolePairs, K)

if ~isrow(K)
  K = K';
end

Slots = 0:size(K,2)-1;
NumSlot = length(Slots);
SlotAngle = 2*pi/NumSlot;
ElSlotAngle = PolePairs * SlotAngle;
phasors = exp(1i*Slots.*ElSlotAngle).*K;  
kw = abs(sum(phasors))/sum(abs(phasors));
  
end % function