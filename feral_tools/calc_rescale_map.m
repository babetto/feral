function Map = calc_rescale_map(Map, ncs_new, ncs_old, Lstk_new, Lstk_old)
%%CALC_RESCALE_MAP rescale the map data to the new stack length and turns

k_ncs = ncs_new/ncs_old;
k_stk = Lstk_new/Lstk_old;

Map.Id = Map.Id / k_ncs;
Map.Iq = Map.Iq / k_ncs;

Map.Id_vec = Map.Id(1,:);
Map.Iq_vec = Map.Iq(:,1);

Map.CurrentAmplitude = hypot(Map.Id,Map.Iq);

Map.FluxD = Map.FluxD * k_ncs * k_stk;
Map.FluxQ = Map.FluxQ * k_ncs * k_stk;
Map.Flux = hypot(Map.FluxD, Map.FluxQ);

if isfield(Map, 'TorqueDQ')
  Map.TorqueDQ = Map.TorqueDQ * k_stk;
end
  
if isfield(Map, 'TorqueMXW')
  Map.TorqueMXW = Map.TorqueMXW * k_stk;
end

end % function