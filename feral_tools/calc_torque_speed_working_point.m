function [res, Feasible] = calc_torque_speed_working_point(Map, VoltageLimit, CurrentLimit, PolePairs, WindingResistance, RequiredTorque, RequiredMechSpeedRPM, InterpMethod)
%%CALC_TORQUE_SPEED_WORKING_POINT computes the working point which yields 
% the required torque and speed for a given voltage limit

Rw = WindingResistance;
pp = PolePairs;
Map.Id_vec = Map.Id(1,:);
Map.Iq_vec = Map.Iq(:,1);

TorqueDQ = calc_torque_DQ(pp, Map.Id, Map.Iq, Map.FluxD, Map.FluxQ);

% compute all the (Id,Iq) combinations for T=cost
try
  [Ctorque] = contourc(Map.Id, Map.Iq, TorqueDQ, RequiredTorque * [1,1]);
catch
  [Ctorque] = contourc(Map.Id_vec, Map.Iq_vec, TorqueDQ, RequiredTorque*[1,1]);
end

% save the current values
Id = Ctorque(1,2:end);
Iq = Ctorque(2,2:end);

% make the current arrays finer
Id_fine = linspace(min([Id Iq]), max([Id Iq]), length(Id)*10);
Iq_fine = interp1(Id, Iq, Id_fine, InterpMethod);

% compute the fluxes and torque for each point (Id_fine, Iq_fine)
fluxD = interp2(Map.Id, Map.Iq, Map.FluxD, Id_fine, Iq_fine, InterpMethod);
fluxQ = interp2(Map.Id, Map.Iq, Map.FluxQ, Id_fine, Iq_fine, InterpMethod);

flux = hypot(fluxD, fluxQ);
torq = interp2(Map.Id, Map.Iq, TorqueDQ,  Id_fine, Iq_fine, InterpMethod);

% find mtpa
[~, idx_mtpa] = min(hypot(Id_fine,Iq_fine));
Id_mtpa = Id_fine(idx_mtpa);
Iq_mtpa = Iq_fine(idx_mtpa);
I_mtpa = hypot(Id_mtpa, Iq_mtpa);
alphaie_mtpa = atan2d(Iq_mtpa, Id_mtpa);
alphaie_range = linspace(0.8, 1.2, 1000)*alphaie_mtpa;
fluxD_mtpa = fluxD(idx_mtpa);
fluxQ_mtpa = fluxQ(idx_mtpa);
[~, ~, RPM_mtpa] = calc_speed(VoltageLimit, fluxD_mtpa, fluxQ_mtpa, Id_mtpa, Iq_mtpa, Rw, pp);

% find mtpv
[~, ~, RPM_fw] = calc_speed(VoltageLimit, fluxD, fluxQ, Id_fine, Iq_fine, Rw, pp);
[RPM_mtpv, index_mtpv] = max(RPM_fw);

if index_mtpv > 1 % remove unfeasible solutions
  RPM_fw(1:index_mtpv-1) = [];
  torq(1:index_mtpv-1) = [];
  flux(1:index_mtpv-1) = [];
  Id_fine(1:index_mtpv-1)  = [];
  Iq_fine(1:index_mtpv-1) = [];
end

warning('off','backtrace')
if RPM_mtpa >= RequiredMechSpeedRPM % the required speed is lower than the mtpa speed --> select  MTPA condition
    
    Feasible = 1;

    if I_mtpa > CurrentLimit
      Feasible = 0;
      warning('Working point non feasible!')
      warning(['MTPA current ', num2str(I_mtpa), 'A higher than current limit!'])
    end

    res = calc_mtpa(Map, I_mtpa, alphaie_range, VoltageLimit);
    res.MechSpeedRPM = RequiredMechSpeedRPM;
    res.MechSpeed = 2*pi*RequiredMechSpeedRPM/60;
    res.ElSpeed = res.MechSpeed*pp;
    res.ElFrequency = res.ElSpeed/2/pi;
    [res.Ud, res.Uq, res.U] = calc_voltage(res.Id, res.Iq, res.FluxD, res.FluxQ, Rw, res.ElSpeed);
    res.WorkingPointRegion = 'MTPA';

else

    Feasible = 1;

    [RPM_wp,index] = min(abs(RPM_fw-RequiredMechSpeedRPM));
    Torque = torq(index);
    Id = Id_fine(index);
    Iq = Iq_fine(index);
    Ipeak = hypot(Id,Iq);

    if RequiredMechSpeedRPM > RPM_mtpv % the required speed is higher than the MTPV speed --> no feasible solutions
      warning('Working point non feasible!')
      warning(['MTPV speed ', num2str(RPM_mtpv), 'rpm lower than required speed!'])
      warning('MTPV working point is provided')
      Feasible = 0;
    end

    if Ipeak > CurrentLimit
      warning('Working point non feasible!')
      warning(['Flux weakening current ', num2str(Ipeak), 'A higher than current limit!'])
      Feasible = 0;
    end

    res.Torque = Torque;
    res.Id = Id;
    res.Iq = Iq;
    res.CurrentAmplitude = hypot(res.Id,res.Iq);
    res.CurrentAngle = angle(res.Id + 1i*res.Iq)*180/pi;
    res.FluxD = interp2(Map.Id, Map.Iq, Map.FluxD, res.Id, res.Iq, InterpMethod);
    res.FluxQ = interp2(Map.Id, Map.Iq, Map.FluxQ, res.Id, res.Iq, InterpMethod);
    res.Flux = hypot(res.FluxD,res.FluxQ);
    [res.ElSpeed, res.MechSpeed, res.MechSpeedRPM, res.ElFrequency] = calc_speed(VoltageLimit, res.FluxD, res.FluxQ, res.Id, res.Iq, Rw, pp);
    [res.Ud, res.Uq, res.U] = calc_voltage(res.Id, res.Iq, res.FluxD, res.FluxQ, Rw, res.ElSpeed);
    res.WorkingPointRegion = 'FW-MTPV';

end

warning('on','backtrace')

end

%warning('on','backtrace')