function H_extrap = extrap_BH_point(H, B, B_extrap)

mu_0 = 4e-7*pi;

B_last = B(end);
H_last = H(end);

H_extrap = H_last + (B_extrap - B_last) / mu_0;

end % function