function r = calc_ripple(v)

r = (max(v) - min(v))./mean(v);

end % function