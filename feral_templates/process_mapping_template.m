close all; clear; clc;

load([]) % add here the sim_mapping file results

%% INPUT DATA
Map.VoltageLimit = ; % rated voltage (V) peak
% Map.WindingResistance = ; % winding resistance [ohm]
% Map.EndWindingInductance = 0; % end-winding inductance [H]
Map.MaxMechSpeedRPM = ; % maximum mechanical speed (rpm)
% Map.InterpMethod = 'cubic'; % interpolation method
Map.CurrentAmplitudeRange = []; % current amplitudes array for the MTPA locus
% Map.CurrentLimit = ; % rated current (A) peak
Map.CurrentAngleRange = linspace(90,180,1e3); % current angle array to compute FW and MTPV
% Map.DQReferenceFrame = 'SyR'; % reference frame of the simulation

%% Maximum torque per ampere (MTPA) locus
[mtpa, base] = calc_mtpa(Map, Map.CurrentAmplitudeRange, Map.CurrentAngleRange, Map.VoltageLimit);
                                
%% Flux-weakening (FW) and Maximum torque per volt (MTPV) locus 
[Map.MTPA, Map.FW, Map.MTPV] = calc_mtpa_fw_mtpv(Map, Map.CurrentAmplitudeRange);

%% FIGUREs
plot_sim_mapping_results(Map);