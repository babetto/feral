function material = mtrl_minimal_template()
%MTRL_COPPER defines the properties of the 'copper' material

FunctionName = mfilename;
MaterialName = FunctionName(6:end);

% material.RelativePermeability = [1 1];
% material.CoercitiveField = 0;
% material.AppliedCurrentDensity = 0;
% material.ElConductivity = 0;
% material.LaminationThickness = 0;
% material.HysteresisLagAngle = 0;
% material.LaminationFill = 0;
% material.LaminationType = 0;

% other properties 
% material.SpecificCost = ; % [euro/kg]
% material.MassDensity = ; % mass density of the slot material (kg/m3)

end