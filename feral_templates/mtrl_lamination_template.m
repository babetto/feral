function material = mtrl_lamination_template(Frequency)
%MTRL_LAMINATION_TEMPLATE is a template for a lamination material function

FunctionName = mfilename;
MaterialName = FunctionName(6:end);

% set default frequency is not defined
if nargin < 1
  Frequency = 50;
end

material.Name = [MaterialName,'@',num2str(Frequency),'Hz'];

% iron losses equation:
% losses = Khy * f^alpha * B^beta + Kec * (f*B)^2
% Khy = hysteresis coefficient
% alpha, beta = frequency and flux density coefficients
% Kec = eddy-current coefficient

% chose the proper lamination data
FrequencyData = []; % frequency data array (lamination data sheet)

% default BH curve
Default_BHpoints = [

];


% Selection of the material properties according to the frequency

if Frequency < FrequencyData(2) %  0 < f < 100
  
  material.LossesFreqRef = FrequencyData(1); % [Hz]
  material.HysteresisCoeff = ; % [-]
  material.EddyCurrentCoeff =  ; % [-]
  material.AlphaCoeff = ; % [-]
  material.BetaCoeff = ; % [-]
  
  material.BHpoints = [];
  
elseif Frequency >= FrequencyData(2) && Frequency < FrequencyData(3) %  100 <= f < 200
  
  material.LossesFreqRef = FrequencyData(2); % [Hz]
  material.HysteresisCoeff = ; % [-]
  material.EddyCurrentCoeff = ; % [-]
  material.AlphaCoeff = ; % [-]
  material.BetaCoeff = ; % [-]
  material.BHpoints = []; 
  
elseif Frequency >= FrequencyData(3) %  f >= 200

  material.LossesFreqRef = FrequencyData(3); % [Hz]
  material.HysteresisCoeff = ; % [-]
  material.EddyCurrentCoeff = ; % [-]
  material.AlphaCoeff = ; % [-]
  material.BetaCoeff = ; % [-]
  material.BHpoints =  []; 
  
end

% load default BH curve is no other curves are defined
if ~isfield(material, 'BHpoints')
  material.BHpoints = Default_BHpoints;
end

material.ElResistivity= ; % [uOhm*cm]
material.MassDensity = ; % mass density of the lamination
material.SpecificCost = ; % [euro/kg]

end