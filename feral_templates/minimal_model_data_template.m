%% General data 
MD.ModelName = ' '; % .fem model name 
MD.ScaleFactor = 1e-3; 
MD.StackLength = *MD.ScaleFactor; 
MD.PolePairs = ; 
MD.Airgap = ; 
MD.SimPoles = ; 

%% Stator general data 
Stator.Group = ; 
Stator.Type = 'External';
Stator.PolePairs = MD.PolePairs; 
% Stator.Geometry.FirstSlotAngle = 0;
Stator.Geometry.Airgap = MD.Airgap; 
Stator.Geometry.StackLength = MD.StackLength; 
Stator.Geometry.Slots = ;  
Stator.Geometry.OuterDiameter = *MD.ScaleFactor; 
Stator.Geometry.InnerDiameter = *MD.ScaleFactor; 
Stator.Geometry.ToothWidth = *MD.ScaleFactor; 
Stator.Geometry.SlotHeight = *MD.ScaleFactor; 
Stator.Geometry.SlotOpeningHeight = *MD.ScaleFactor; 
% Stator.Geometry.DiameterHandle = Stator.Geometry.InnerDiameter

%% Stator materials 
Stator.Material.Slot = ; 
Stator.Material.Lamination = ; 

%% Stator winding properties 
Stator.Winding.SlotFillFactor = ;  
Stator.Winding.ConductorsInSlot = ; 
Stator.Winding.ParallelPaths = ; 
Stator.Winding.SlotMatrix = []; 
% Stator.Winding.CircName = 'Islot';

%% Rotor general data 
Rotor.Group = ; 
Rotor.Type = 'Internal';
Rotor.IronGroup = ;
Rotor.MagnetGroups = []; % (for fft losses) 
Rotor.Alignment = ; % [deg]
Rotor.PolePairs = MD.PolePairs; 
Rotor.Geometry.Airgap = MD.Airgap; 
Rotor.Geometry.StackLength = MD.StackLength; 
Rotor.Geometry.OuterDiameter = *MD.ScaleFactor;
Rotor.Geometry.InnerDiameter = *MD.ScaleFactor; 
% Rotor.Geometry.DiameterHandle = Rotor.Geometry.OuterDiameter

%% Rotor materials 
Rotor.Material.Lamination = ; 
Rotor.Material.Magnet = ; 

%% Final structure
MD.Stator = Stator; 
MD.Rotor = Rotor; 
MD.MotionGroups = []; 

%% Further options
% MD.ModelPath = '';
% MD.RotorPositions = [];
... add here further settings